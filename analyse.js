const scanner = require("sonarqube-scanner");

scanner(
  {
    // this example uses local instance of SQ
    serverUrl: "http://localhost:9000/",
    options: {
      "sonar.projectVersion": "2.2.0",
      "sonar.sources": "src",
      "sonar.login": "9f7014d218128457ccbaf71df25e0efa9071f1bb"
    },
  },
  () => {
    // callback is required
  }
);
